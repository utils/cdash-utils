#!/usr/bin/env python3
'''
Update CDash dynamic groups for CI configurations

This script updates CDash dynamic groups to contain a set of builds. Managing
this via the web interface has proven to be tedious, but CDash has an API that
lets us do this programmatically.

In order to perform this, an API token is required. By default, the token will
be read from a `.cdash-token` file beside the JSON file provided by `-g`. If
this does not exist, the `-t` flag must be used to provide the token. An API
token may be obtained from the CDash website by navigating:

  - logging into CDash
  - go to the "My CDash" link at the top
  - at the bottom of this page, add a description for the token and obtain it
    via the "Generate Token" button

Other than a token, this script requires at least two arguments:

  - `-p`: The CDash project to modify
  - `-g`: The JSON file describing the dynamic groups and their member builds

The structure of the groups JSON file is a top-level object where the keys are
the names of the dynamic groups and whose values are a list of objects
describing the builds to add to that dynamic group. Each build object must have
the following keys:

  - `group`: the group the build submits to
  - `site`: the name of the site which performs the build
  - `buildname`: a unique substring of the build's name

Other options accepted by the script:

  - `-c`: The CDash instance to talk to (defaults to `open.cdash.org`)
  - `-d`: The date to look up sites in (in `YYYY-MM-DD` format)

Sometimes a CDash group might be missing a site from the current day's
submissions. Older submissions may be used by providing a date in the past
instead.
'''

import argparse
import json
import os.path
import requests


class Build(object):
    def __init__(self, group, site, buildname):
        self.site = site
        self.group = group
        self.buildname = buildname

    def for_project(self, project):
        # Find the group id.
        groupid = None
        for buildgroup in project['all_buildgroups']:
            if buildgroup['name'] == self.group:
                groupid = buildgroup['id']

        if groupid is None:
            raise RuntimeError(f'no such buildgroup: {self.group}')

        return {
            'match': self.buildname,
            'site': self.site,
            'parentgroupid': groupid,
        }


def options():
    p = argparse.ArgumentParser(
        description='Manage dynamic build groups on a CDash project')
    p.add_argument('-c', '--cdash', type=str,
                   default='https://open.cdash.org',
                   help='the CDash instance')
    p.add_argument('-d', '--date', type=str,
                   help='the date to use when querying CDash')
    p.add_argument('-p', '--project', type=str, required=True,
                   help='the name of the project on CDash')
    p.add_argument('-g', '--groups', type=str, required=True,
                   help='the JSON file describing the dynamic build groups')
    p.add_argument('-t', '--token', type=str,
                   help='the API token to use for the project')

    return p


def cdash_url(instance, path):
    return f'{instance}/api{path}'


if __name__ == '__main__':
    p = options()
    opts = p.parse_args()

    # Read the token.
    if opts.token is None:
        dirname = os.path.dirname(os.path.realpath(opts.groups))
        api_key_file = os.path.join(dirname, '.cdash-token')
        if not os.path.exists(api_key_file):
            raise RuntimeError(
                'could not find `.cdash-token` beside the groups file and no '
                'token provided')
        with open(api_key_file, 'r') as fin:
            opts.token = fin.read().strip()

    # Make the instance a full URL.
    if not opts.cdash.startswith('https://') and \
       not opts.cdash.startswith('http://'):
        opts.cdash = f'https://{opts.cdash}'

    # Fetch information about the project.
    project_url = cdash_url(opts.cdash, '/v1/index.php')
    params = {'project': opts.project}

    if opts.date is not None:
        params['date'] = opts.date

    rsp = requests.get(project_url, params=params)
    project = rsp.json()

    headers = {'Authorization': f'Bearer {opts.token}'}

    # Construct buildgroup information.
    buildgroup_url = cdash_url(opts.cdash, '/v1/buildgroup.php')

    buildgroups = {}
    with open(opts.groups, 'r') as fin:
        build_descs = json.load(fin)
        for group, items in build_descs.items():
            buildgroups[group] = []
            for item in items:
                buildgroups[group].append(Build(**item))

    for target_group, builds in buildgroups.items():
        buildgroupid = None
        for buildgroup in project['all_buildgroups']:
            if buildgroup['name'] == target_group:
                buildgroupid = buildgroup['id']
                break

        if buildgroupid is None:
            raise RuntimeError(f'no such buildgroup: {target_group}')

        payload = {
            'projectid': project['projectid'],
            'buildgroupid': buildgroupid,
        }

        # Construct the appropriate item for this build.
        dynamiclist = []
        for build in builds:
            dynamiclist.append(build.for_project(project))

        payload['dynamiclist'] = dynamiclist

        requests.put(buildgroup_url, json=payload, headers=headers)
